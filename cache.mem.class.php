<?php
/**
* Author:
* Bernard Szlachta
* bernard.szlachta@nobleprog.com
* www.nobleprog.co.uk
*
* 
* This software is GPL Licened
**/

// This class uses Druapl cache API 
class Cache {
	private $mysql; // instance of MySQL object
	private $result; // instance of Result object
	private $expiry; // cache expire time in seconds
	private $cacheTag; // cache file
	private $data; // result set array
	private $query; // DEBUG purpouse only, after testing, comment it
	public $cache_table = 'cache_query';

	public $num_rows; //only for compatibility with mysqli

	function Cache(& $mysql, $expiry, $cacheTag) {
		$this->mysql = & $mysql;
		$this->cacheTag = $cacheTag;
		$this->data = array ();
		$this->expiry = $expiry;
	}

	function query($query) {
		//	echo $query;
		$this->query = $query;

		if (!preg_match("/^select/", trim(strtolower($query))) // not DQL
		or $this->expiry <= 0) { // not cached queries
			$this->result = $this->mysql->query($query . " /* not cached */");
			return $this->result;
		}

		// Is it in the cache already
		if ($data = cache_get($this->cacheTag, $this->cache_table)) {
			$this->data = $data->data;
			$this->count_rows();
		} else { // Write the cache and return data
			$this->result = $this->mysql->query($query . "/* caching */");
			$this->data = $this->write();
			$this->count_rows();
		}
		return $this;
	}

	function write() {
		while ($row = mysqli_fetch_array($this->result, MYSQL_BOTH)) {
			$content[] = $row;
		}
		cache_set($this->cacheTag, $this->cache_table, $content, time() + $this->expiry);
		
		return $content;
	}


	function fetch_row() {
		if (!$this->data)
			return false;

		if (!$row = current($this->data)) {
			return false;
		}
		next($this->data);
		return $row;
	}

	function fetch_assoc() {
		return $this->fetch_row();
	}

	function fetch_array() {
		return $this->fetch_row();
	}

	function count_rows() {
		$rows = count($this->data);
		$this->num_rows = $rows;
		return $rows;
	}

	function fetch_object() {
		$assoc = $this->fetch_assoc();
		return $this->array2object($assoc);
	}

	/**
	 * Helper function which changes array into object
	 */
	private function array2object($array) {
		if (is_array($array)) {
			$obj = new StdClass();

			foreach ($array as $key => $val) {
				$obj-> $key = $val;
			}
		} else {
			$obj = $array;
		}
		return $obj;
	}
} // End of the class