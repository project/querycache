Index: hitra/trunk/www/includes/database.mysqli.inc
===================================================================
--- hitra/trunk/www/includes/database.mysqli.inc (revision 338)
+++ hitra/trunk/www/includes/database.mysqli.inc (revision 348)
@@ -77,6 +77,5 @@
   $url['path'] = urldecode($url['path']);
 
-  $connection = mysqli_init();
-  @mysqli_real_connect($connection, $url['host'], $url['user'], $url['pass'], substr($url['path'], 1), $url['port'], NULL, MYSQLI_CLIENT_FOUND_ROWS);
+  $connection = new mysqli($url['host'], $url['user'], $url['pass'], substr($url['path'], 1), $url['port'], NULL);
 
   // Find all database connection errors and error 1045 for access denied for user account
@@ -114,5 +113,5 @@
 
   /* Force UTF-8 */
-  mysqli_query($connection, 'SET NAMES "utf8"');
+  $connection->query('SET NAMES "utf8"');
 
   return $connection;
@@ -129,6 +128,11 @@
     $timer = (float)$usec + (float)$sec;
   }
-
-  $result = mysqli_query($active_db, $query);
+  
+  global $QUERY_CACHE;
+  if($QUERY_CACHE){
+    $result = runquery($query);
+  } else {
+    $result = $active_db->query($query);
+  }
 
   if (variable_get('dev_query', 0)) {
@@ -145,5 +149,5 @@
   }
 
-  if (!mysqli_errno($active_db)) {
+  if (!$active_db->errno) {
     return $result;
   }
@@ -165,5 +169,6 @@
 function db_fetch_object($result) {
   if ($result) {
-    return mysqli_fetch_object($result);
+    $object = $result->fetch_object();
+    return $object;
   }
 }
@@ -181,5 +186,5 @@
 function db_fetch_array($result) {
   if ($result) {
-    return mysqli_fetch_array($result, MYSQLI_ASSOC);
+    return $result->fetch_array(MYSQLI_ASSOC);
   }
 }
@@ -195,5 +200,5 @@
 function db_num_rows($result) {
   if ($result) {
-    return mysqli_num_rows($result);
+    return $result->num_rows;
   }
 }
@@ -213,6 +218,6 @@
 */
 function db_result($result, $row = 0) {
-  if ($result && mysqli_num_rows($result) > $row) {
-    $array = mysqli_fetch_array($result, MYSQLI_NUM);
+  if ($result && $result->num_rows > $row) {
+    $array = $result->fetch_array(MYSQLI_NUM);
     return $array[0];
   }
