/**
* Author:
* Bernard Szlachta
* bernard.szlachta@nobleprog.com
* www.nobleprog.co.uk
*
* 
* This software is GPL Licened
**/


Features:
1. It works only with mysqli driver because of its object orientation (I simply cannot write good code in different way)
2. It increase performance only with memcache as the cacheapi backend

INSTALLATION STEPS
1. Patch the includes/database.mysqli.inc using apropriate patches
	This patch changes MySQLi procedural style function to object oriented
2. Enable querycache module
3. Add configuration variables and function to your setting.php
4. Set the policy based on regular expression

Sample setting.php confiuration

// Enable or disable query cache
global $QUERY_CACHE;
$QUERY_CACHE = true;

require_once('modules/querycache/querycache.module');

function getCacheQueryExpiryTime($sql)
{
	if(ereg(".*FROM system.*",trim($sql))) return 60;
	if(ereg(".*FROM role.*",trim($sql))) return 60;
	if(ereg(".*FROM access.*",trim($sql))) return 60;

    return 0;	// If not found, do not cache
}

More information and support is available at www.nobleprog.co.uk